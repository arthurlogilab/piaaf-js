/* eslint-env mocha */
/* global describe, it, Response */
import { expect } from 'chai';

import * as sparql from '../src/sparql';
import * as graph from '../src/components/graph';
import * as home from '../src/components/home';

describe('sparql', () => {
    describe('describeJsonGraph2relations', () => {
        it('transform describe bindings to high-level structure', done => {
            const results = sparql.describeJsonGraph2relations('FRBNF_NP_protoLD_DGEARC_0003', [
                {
                    s: { type: 'uri', value: 'FRBNF_NP_protoLD_DGEARC_0003' },
                    p: { type: 'uri', value: 'http://www.w3.org/2004/02/skos/core#altLabel' },
                    o: { type: 'literal', value: 'Biblioth\u00E8que imp\u00E9riale' },
                },
                {
                    s: { type: 'uri', value: 'FRBNF_NP_protoLD_DGEARC_0003' },
                    p: { type: 'uri', value: 'http://www.w3.org/2004/02/skos/core#altLabel' },
                    o: { type: 'literal', value: 'BN' },
                },
                {
                    s: { type: 'uri', value: 'FRBNF_NP_protoLD_DGEARC_0003' },
                    p: { type: 'uri', value: 'http://www.w3.org/2004/02/skos/core#prefLabel' },
                    o: { type: 'literal', value: 'Biblioth\u00E8que nationale (France)' },
                },
                {
                    s: { type: 'uri', value: 'FRBNF_NP_protoLD_DGEARC_0001' },
                    p: { type: 'uri', value: 'http://www.w3.org/1999/02/22-rdf-syntax-ns#type' },
                    o: { type: 'uri', value: 'FRBNF_NP_protoLD_DGEARC_0003' },
                },
                {
                    s: { type: 'uri', value: 'FRBNF_NP_protoLD_DGEARC_0003' },
                    p: { type: 'uri', value: 'http://www.europeana.eu/schemas/edm/end' },
                    o: { type: 'literal', value: '1994' },
                },
                {
                    s: { type: 'uri', value: 'FRBNF_NP_protoLD_DGEARC_0003' },
                    p: { type: 'uri', value: 'http://www.europeana.eu/schemas/edm/start' },
                    o: { type: 'literal', value: '1792' },
                },
                {
                    s: { type: 'uri', value: 'FRBNF_NP_protoLD_DGEARC_0003' },
                    p: { type: 'uri', value: 'http://www.w3.org/1999/02/22-rdf-syntax-ns#type' },
                    o: { type: 'uri', value: 'http://www.europeana.eu/schemas/edm/Agent' },
                },
            ], []);
            expect(results).to.deep.equal({
                attributes: {
                    'http://www.europeana.eu/schemas/edm/end': ['1994'],
                    'http://www.europeana.eu/schemas/edm/start': ['1792'],
                    'skos:altLabel': ['Bibliothèque impériale', 'BN'],
                    'skos:prefLabel': ['Bibliothèque nationale (France)'],
                },
                'subject-relations': {
                    'rdf:type': [
                        [
                            'http://www.europeana.eu/schemas/edm/Agent',
                            'http://www.europeana.eu/schemas/edm/Agent',
                        ],
                    ],
                },
                'packed-relations': {},
                'object-relations': {
                    'rdf:type': [['FRBNF_NP_protoLD_DGEARC_0001', 'FRBNF_NP_protoLD_DGEARC_0001']],
                },
            });
            done();
        });
    });

    describe('sparqlGraph2SigmaGraphData', () => {
        it('transform custom query bindings to sigma graph structure', done => {
            const results = sparql.sparqlGraph2SigmaGraphData(
                [
                    {
                        entity: { value: 'http://www.piaaf.net/agents/FRAN_corporate-body_005061' },
                        entityLabel: { value: "Resource's label" },
                        rel1: {
                            value: 'http://www.ica.org/standards/RiC/ontology#agentControlledBy',
                        },
                        target: { value: 'http://www.piaaf.net/agents/FRAN_corporate-body_000005' },
                    },
                    {
                        entity: { value: 'http://www.piaaf.net/agents/FRAN_corporate-body_005061' },
                        rel1: {
                            value:
                                'http://www.ica.org/standards/RiC/ontology#agentIsInferiorInHierarchicalRelation',
                        },
                        target: { value: 'http://www.piaaf.net/agents/FRAN_corporate-body_005437' },
                        targetLabel: { value: "Target's label" },
                    },
                    {
                        entity: { value: 'http://www.piaaf.net/agents/FRAN_corporate-body_005061' },
                        rel1: { value: 'http://www.ica.org/standards/RiC/ontology#leadBy' },
                        target: { value: 'http://www.piaaf.net/agents/FRAN_person_050312' },
                    },
                    {
                        entity: { value: 'http://www.piaaf.net/agents/FRAN_corporate-body_005061' },
                        rel1: { value: 'http://www.ica.org/standards/RiC/ontology#groupHasMember' },
                        target: { value: 'http://www.piaaf.net/agents/FRAN_person_050312' },
                    },
                ],
                node => node.id === 'http://www.piaaf.net/agents/FRAN_corporate-body_005061',
            );
            // remove some display only information such as color
            Array.prototype.map.call(results.edges, edge => {
                delete edge.beginningDate;
                delete edge.endDate;
                delete edge.color;
                delete edge.rdfGraph;
                delete edge.size;
                delete edge.type;
            });
            Array.prototype.map.call(results.nodes, node => {
                delete node.color;
                delete node.fromDate;
                delete node.rdfGraphs;
                delete node.rdfType;
                delete node.size;
                delete node.toDate;
                delete node.type;
            });
            expect(results).to.deep.equal({
                nodes: [
                    {
                        _label: "Resource's label",
                        id: 'http://www.piaaf.net/agents/FRAN_corporate-body_005061',
                        fromBaseSet: true,
                        x: 0,
                        y: 0,
                    },
                    {
                        _label: 'http://www.piaaf.net/agents/FRAN_corporate-body_000005',
                        id: 'http://www.piaaf.net/agents/FRAN_corporate-body_000005',
                        fromBaseSet: false,
                        x: 1,
                        y: 1,
                    },
                    {
                        _label: "Target's label",
                        id: 'http://www.piaaf.net/agents/FRAN_corporate-body_005437',
                        fromBaseSet: false,
                        x: 2,
                        y: 0,
                    },
                    {
                        _label: 'http://www.piaaf.net/agents/FRAN_person_050312',
                        id: 'http://www.piaaf.net/agents/FRAN_person_050312',
                        fromBaseSet: false,
                        x: 3,
                        y: 1,
                    },
                ],
                edges: [
                    {
                        id: 0,
                        rdfType: 'ric:agentControlledBy',
                        source: 'http://www.piaaf.net/agents/FRAN_corporate-body_005061',
                        target: 'http://www.piaaf.net/agents/FRAN_corporate-body_000005',
                    },
                    {
                        id: 1,
                        rdfType: 'ric:agentIsInferiorInHierarchicalRelation',
                        source: 'http://www.piaaf.net/agents/FRAN_corporate-body_005061',
                        target: 'http://www.piaaf.net/agents/FRAN_corporate-body_005437',
                    },
                    {
                        id: 2,
                        rdfType: 'ric:leadBy',
                        source: 'http://www.piaaf.net/agents/FRAN_corporate-body_005061',
                        target: 'http://www.piaaf.net/agents/FRAN_person_050312',
                    },
                    {
                        id: 3,
                        rdfType: 'ric:groupHasMember',
                        source: 'http://www.piaaf.net/agents/FRAN_corporate-body_005061',
                        target: 'http://www.piaaf.net/agents/FRAN_person_050312',
                    },
                ],
            });
            done();
        });

        it('assign single color to same property', done => {
            const results = sparql.sparqlGraph2SigmaGraphData(
                [
                    {
                        entity: { value: 'http://www.piaaf.net/agents/FRAN_corporate-body_005061' },
                        rel1: {
                            value: 'http://www.ica.org/standards/RiC/ontology#agentControlledBy',
                        },
                        target: { value: 'http://www.piaaf.net/agents/FRAN_corporate-body_000005' },
                    },
                    {
                        entity: { value: 'http://www.piaaf.net/agents/FRAN_corporate-body_005061' },
                        rel1: {
                            value: 'http://www.ica.org/standards/RiC/ontology#agentControlledBy',
                        },
                        target: { value: 'http://www.piaaf.net/agents/FRAN_corporate-body_005437' },
                    },
                    {
                        entity: { value: 'http://www.piaaf.net/agents/FRAN_corporate-body_005061' },
                        rel1: { value: 'http://www.ica.org/standards/RiC/ontology#leadBy' },
                        target: { value: 'http://www.piaaf.net/agents/FRAN_person_050312' },
                    },
                ],
                _ => false,
            );
            expect(results.edges[0].color).to.equal(results.edges[1].color);
            expect(results.edges[0].color).to.not.equal(results.edges[2].color);
            done();
        });
    });
});

describe('graph', () => {
    it('safeMin', done => {
        expect(graph.safeMin(null, null)).to.equal(null);
        expect(graph.safeMin(null, 1)).to.equal(1);
        expect(graph.safeMin(1, null)).to.equal(1);
        expect(graph.safeMin(1, 0)).to.equal(0);
        done();
    });

    it('outerRange', done => {
        expect(graph.outerRange(null, null)).to.equal(null);
        expect(graph.outerRange(null, [1, 2])).to.deep.equal([1, 2]);
        expect(graph.outerRange([1, 2], null)).to.deep.equal([1, 2]);
        expect(graph.outerRange([1, 3], [0, 2])).to.deep.equal([0, 3]);
        done();
    });

    it('rangeOverlap', done => {
        expect(graph.rangeOverlap([null, null], null)).to.equal(true);
        expect(graph.rangeOverlap([null, null], [10, 20])).to.equal(true);
        expect(graph.rangeOverlap([null, 9], [10, 20])).to.equal(false);
        expect(graph.rangeOverlap([9, null], [10, 20])).to.equal(true);
        expect(graph.rangeOverlap([null, 21], [10, 20])).to.equal(true);
        expect(graph.rangeOverlap([21, null], [10, 20])).to.equal(false);
        expect(graph.rangeOverlap([9, 21], [10, 20])).to.equal(true);
        expect(graph.rangeOverlap([11, 19], [10, 20])).to.equal(true);
        expect(graph.rangeOverlap([9, 10], [10, 20])).to.equal(true);
        expect(graph.rangeOverlap([20, 21], [10, 20])).to.equal(true);
        done();
    });
});

describe('home', () => {
    it('extractParamValue', done => {
        expect(home.extractParamValue(null, 'q')).to.equal(null);
        expect(home.extractParamValue('?q=2', 'q')).to.equal('2');
        expect(home.extractParamValue('?q=%2C', 'q')).to.equal(',');
        expect(home.extractParamValue('?b=%2C', 'q')).to.equal(null);
        done();
    });
});
