import * as d3 from 'd3';
import { min, max } from 'd3-array';
import { scaleLinear } from 'd3-scale';
import { linkHorizontal, linkVertical } from 'd3-shape';

import { localResourceURI } from './sparql';

export const MAX_YEAR = 2030;
// safety belt for htl : number of cycles before we stop trying to converge when
// computing node layout
const MAX_LOOP_CYCLES = 300;

function htlLinkHorizontal() {
    function linkCoordinate(d, sourceOrTarget) {
        // special cases of a box "including" the other
        if (
            d.following.left < d.preceding.left &&
            d.following.left + d.following.width > d.preceding.left + d.preceding.width
        ) {
            // preceding included in following
            let targetTop;
            if (d.preceding.topIndex > d.following.topIndex) {
                targetTop = d.following.top;
            } else {
                targetTop = d.following.top + d.following.height;
            }
            if (sourceOrTarget === 'source') {
                return [
                    d.preceding.left + d.preceding.width,
                    d.preceding.top + d.preceding.height / 2,
                ];
            } else {
                return [d.graph.yearToXCoordinate(d.begin), targetTop];
            }
        } else if (
            d.preceding.left < d.following.left &&
            d.preceding.left + d.preceding.width > d.following.left + d.following.width
        ) {
            // following included in preceding
            let sourceTop;
            if (d.preceding.topIndex > d.following.topIndex) {
                sourceTop = d.preceding.top + d.preceding.height;
            } else {
                sourceTop = d.preceding.top;
            }
            if (sourceOrTarget === 'source') {
                return [d.graph.yearToXCoordinate(d.begin), sourceTop];
            } else {
                return [d.following.left, d.following.top + d.following.height / 2];
            }
            // general case
        } else if (sourceOrTarget === 'source') {
            return [d.preceding.left + d.preceding.width, d.preceding.top + d.preceding.height / 2];
        } else {
            return [
                max([d.following.left, d.preceding.left + d.preceding.width]),
                d.following.top + d.following.height / 2,
            ];
        }
    }

    return linkHorizontal()
        .source(d => linkCoordinate(d, 'source'))
        .target(d => linkCoordinate(d, 'target'));
}

function htlLinkVertical() {
    return linkVertical()
        .source(d => [
            d.hierarchicalParent.left + d.hierarchicalParent.width / 2,
            d.hierarchicalParent.top + d.hierarchicalParent.height,
        ])
        .target(d => [
            min([
                d.hierarchicalChild.left + d.hierarchicalChild.width,
                max([d.hierarchicalChild.left, d.graph.yearToXCoordinate(d.begin)]),
            ]),
            d.hierarchicalChild.top,
        ]);
}

function hierarchicalParents(node) {
    return Array.prototype.map.call(node.hierarchicalParentLinks, link => link.hierarchicalParent);
}
function sortLinksHierarchicallyOn(prop) {
    return function sortLinksHierarchically(l1, l2) {
        if (hierarchicalParents(l1[prop]).indexOf(l2[prop]) > -1) {
            return -1;
        }
        if (hierarchicalParents(l2[prop]).indexOf(l1[prop]) > -1) {
            return 1;
        }
        return 0;
    };
}

function layoutHierarchicalTimeline(graph, mainUri) {
    const width = 1100,
        boxHeight = 70,
        lineSpacing = 20,
        boxPadding = 10,
        margin = 10,
        maxYear = max(graph.nodes, n => n.end),
        minYear = min(graph.nodes, n => n.begin),
        yearWidth = width / (maxYear - minYear);

    let mainNode = null;

    graph.yearToXCoordinate = year => margin + (year - minYear) * yearWidth;

    // set nodes in the X axis (left + width), according to their begin/end date
    for (const node of graph.nodes) {
        node.left = graph.yearToXCoordinate(node.begin);
        node.width = (node.end - node.begin) * yearWidth;
        const thisBoxPadding = min([boxPadding, (node.end - node.begin) * yearWidth / 3]);
        if (node.precedingLinks.length > 0) {
            node.left += thisBoxPadding;
            node.width -= thisBoxPadding;
        }
        if (node.followingLinks.length > 0) {
            node.width -= thisBoxPadding;
        }
        // search for the main node
        if (node.uri === mainUri) {
            mainNode = node;
        }

        // sort following / preceding links according to hiearchical
        // information: we want link to nodes upper in the hierarchy after
        // others
        node.followingLinks.sort(sortLinksHierarchicallyOn('following'));
        node.precedingLinks.sort(sortLinksHierarchicallyOn('preceding'));
    }
    // bind graph to links, necessary in htlLink* to access yearToXCoordinate
    graph.hierarchicalLinks.forEach(link => {
        link.graph = graph;
    });
    graph.chronologicalLinks.forEach(link => {
        link.graph = graph;
    });

    // set nodes in the Y axis (top + height) in three steps, using a 'topIndex'
    // attribute indicating the "line" number on which the node should be
    // displayed:
    //
    // - initialization, set topIndex of main node and its hierarchical descendant
    if (mainNode.hierarchicalChildLinks.length > 0) {
        mainNode.topIndex = 1;
        for (const link of mainNode.hierarchicalChildLinks) {
            link.hierarchicalChild.topIndex = 0;
        }
    } else {
        mainNode.topIndex = 0;
    }
    // - first step, set topIndex of nodes at the same level as the start node to 0
    setLineTopIndex(mainNode, 'preceding');
    setLineTopIndex(mainNode, 'following');

    // - second step, handle remaining nodes without index, linked to an already
    //   indexed node through hierarchical relations, until there are no remaining
    //   unindexed nodes
    let idx;
    for (idx = 0; idx < MAX_LOOP_CYCLES; idx++) {
        let mayNeedMoreLoop = false;
        for (const node of graph.nodes) {
            if (node.topIndex === undefined) {
                // search for a child node with topIndex set
                const indexedChild = firstFollowingTopIndexedNode(node, 'hierarchicalChild');
                if (indexedChild !== null) {
                    // if found, set topIndex of its parents, `node` included
                    setParentsTopIndex(indexedChild);
                } else {
                    // else search of a sibbling (preceding or following) with
                    // topIndex set
                    const indexedSibbling =
                        firstFollowingTopIndexedNode(node, 'preceding') ||
                        firstFollowingTopIndexedNode(node, 'following');
                    if (indexedSibbling !== null) {
                        // if found, set topIndex of its left and right
                        // sibblings, `node` included
                        setLineTopIndex(indexedSibbling, 'preceding');
                        setLineTopIndex(indexedSibbling, 'following');
                    }
                    // else we should be reached by later iteration
                }
                mayNeedMoreLoop = true;
            }
        }
        if (!mayNeedMoreLoop) {
            break;
        }
    }
    // if we stop looping because MAX_LOOP_CYCLES was reached, then set
    // arbitrarily topIndex to all nodes lacking a value. This won't provide
    // a good layout but will avoid propagating NaN everywhere downwards.
    if (idx === MAX_LOOP_CYCLES) {
        for (const node of graph.nodes) {
            if (node.topIndex === undefined) {
                node.topIndex = 0;
            }
        }
    }
    // - third step, resolve conflicts (nodes with the same top index and overlapping year range)
    for (let idx = 0; idx < MAX_LOOP_CYCLES; idx++) {
        let mayNeedMoreLoop = false;
        for (const node1 of graph.nodes) {
            for (const node2 of graph.nodes) {
                if (
                    node1 !== node2 &&
                    node1.topIndex === node2.topIndex &&
                    (node1.begin < node2.end && node1.end > node2.begin)
                ) {
                    // move up one of the node, considering their potential hiearchical relation
                    if (hierarchicalParents(node1).indexOf(node2) > -1) {
                        node2.topIndex += 1;
                    } else {
                        node1.topIndex += 1;
                    }
                    mayNeedMoreLoop = true;
                }
            }
        }
        if (!mayNeedMoreLoop) {
            break;
        }
    }

    // finally, turn index into coordinate
    const yLevels = max(graph.nodes, node => node.topIndex);
    for (const node of graph.nodes) {
        node.top = margin + (yLevels - node.topIndex) * (boxHeight + lineSpacing);
        node.height = boxHeight;
    }
    // compute overall graph size
    graph.width = (maxYear - minYear) * yearWidth + 2 * margin;
    graph.height = (yLevels + 1) * (boxHeight + lineSpacing) + 2 * margin;
    graph.minYear = minYear;
    graph.maxYear = maxYear;
    return graph;
}

// Set index on all of preceding/following nodes of the given `node`, depending
// on the `baseKey` value (one of 'preceding' or 'following'). When a node as
// several predecessor or followers, index are incremented so they will be
// dispatched vertically.
//
// Returns the maximum index attributed.
function setLineTopIndex(node, baseKey) {
    let maxIndex = 0;
    const stack = [node];
    while (stack.length > 0) {
        const current = stack.pop();
        current[baseKey + 'Links'].forEach((link, i) => {
            const nextNode = link[baseKey];
            if (nextNode.topIndex === undefined) {
                nextNode.topIndex = current.topIndex + i;
                stack.push(nextNode);
                if (nextNode.topIndex > maxIndex) {
                    maxIndex = nextNode.topIndex;
                }
            }
        });
    }
    return max([node.topIndex, maxIndex]);
}

// Set index on all parents nodes of the given `node`. The whole `nodes` list is
// given to allow "decaying" those with index greater or equal to newly attributed
// index (to avoid collision).
//
// Returns the maximum index attributed.
function setParentsTopIndex(node) {
    const stack = [node];
    let idx = 0;
    while (++idx < MAX_LOOP_CYCLES && stack.length > 0) {
        const current = stack.pop();
        for (const link of current.hierarchicalParentLinks) {
            const nextNode = link.hierarchicalParent;
            if (nextNode.topIndex === undefined) {
                nextNode.topIndex = current.topIndex + 1;
                stack.push(nextNode);
                // in case there are yet-not-handled sibling nodes
                setLineTopIndex(nextNode, 'preceding');
                setLineTopIndex(nextNode, 'following');
            }
        }
    }
}

// Return the first node with topIndex set by following axis specified through
// `baseKey`. Return null if none found.
function firstFollowingTopIndexedNode(node, baseKey) {
    const stack = [node];
    let idx = 0;
    while (++idx < MAX_LOOP_CYCLES && stack.length > 0) {
        const current = stack.pop(),
            links = current[baseKey + 'Links'];
        for (let i = 0; i < links.length; i++) {
            const link = links[i],
                nextNode = link[baseKey];
            if (nextNode.topIndex !== undefined) {
                return nextNode;
            }
            stack.push(nextNode);
        }
    }
    return null;
}

export function drawHierarchicalTimelineSVG(svgSelector, uri, graph, history) {
    layoutHierarchicalTimeline(graph, uri);
    const svg = d3.select(svgSelector);

    let node = svg
            .append('g')
            .attr('class', 'nodes')
            .attr('font-family', 'sans-serif')
            .attr('font-size', 10)
            .selectAll('g'),
        clink = svg
            .append('g')
            .attr('class', 'clinks')
            .attr('fill', 'none')
            .attr('stroke', '#22FF22')
            .attr('stroke-opacity', 0.2)
            .selectAll('path'),
        hlink = svg
            .append('g')
            .attr('class', 'hlinks')
            .attr('fill', 'none')
            .attr('stroke-opacity', 0.2)
            .selectAll('path');

    clink = clink
        .data(graph.chronologicalLinks)
        .enter()
        .append('path')
        .attr('class', 'link')
        .on('click', d => history.push(localResourceURI(d.uri)))
        .on('mouseover', function () {
            d3.select(this).style('cursor', 'pointer');
        })
        .on('mouseout', function () {
            d3.select(this).style('cursor', 'default');
        })
        .attr('d', htlLinkHorizontal())
        .attr('stroke-width', 10);
    clink.append('title').text(d => d.begin);

    hlink = hlink
        .data(graph.hierarchicalLinks)
        .enter()
        .append('path')
        .on('click', d => history.push(localResourceURI(d.uri)))
        .on('mouseover', function () {
            d3.select(this).style('cursor', 'pointer');
        })
        .on('mouseout', function () {
            d3.select(this).style('cursor', 'default');
        })
        .attr('class', 'link')
        .attr('d', htlLinkVertical())
        .attr('stroke', d => (d.relationType === 'control' ? '#2222FF' : '#FF2222'))
        .attr('stroke-width', 5);
    hlink.append('title').text(d => formatDateRange(d));

    node = node
        .data(graph.nodes)
        .enter()
        .append('g');
    node
        .append('rect')
        .on('click', d => history.push(localResourceURI(d.uri)))
        .on('mouseover', function () {
            d3.select(this).style('cursor', 'pointer');
        })
        .on('mouseout', function () {
            d3.select(this).style('cursor', 'default');
        })
        .attr('x', d => d.left)
        .attr('y', d => d.top)
        .attr('height', d => d.height)
        .attr('width', d => d.width)
        .style('stroke', '#2c3e50')
        .style('fill', d => (d.uri === uri ? '#d2d2ff' : 'white'))
        .style('stroke-width', 1);
    node
        .append('text')
        .filter(d => d.width > 50)
        .on('click', d => history.push(localResourceURI(d.uri)))
        .on('mouseover', function () {
            d3.select(this).style('cursor', 'pointer');
        })
        .on('mouseout', function () {
            d3.select(this).style('cursor', 'default');
        })
        .attr('x', d => d.left + d.width / 2)
        .attr('y', d => d.top + d.height / 2)
        .attr('dy', '.35em')
        .attr('text-anchor', 'middle')
        .text(d => d.label.split('(')[0].replace('France.', ''));
    node.append('title').text(d => `${d.label} [${formatDateRange(d)}]`);
    svg.selectAll('g.nodes text').each(d3EnhanceBoxLabels);

    // Add the X axis
    const xScale = scaleLinear()
            .range([0, graph.width])
            .domain([graph.minYear, min([new Date().getFullYear(), graph.maxYear])]),
        xAxis = d3
            .axisBottom()
            .scale(xScale)
            .ticks(5, 'd');
    svg
        .append('g')
        .attr('class', 'x axis')
        .attr('transform', `translate(0, ${graph.height})`)
        .call(xAxis);

    function markerPath(d) {
        const x0 = xScale(d.year),
            y0 = graph.height;
        const path = [`M${x0} ${y0}`];
        path.push(`L ${x0 - 5} ${y0 - 5}`);
        path.push(`L ${x0 - 5} ${y0 - 15}`);
        path.push(`L ${x0 + 5} ${y0 - 15}`);
        path.push(`L ${x0 + 5} ${y0 - 5}`);
        path.push('Z');
        return path.join(' ');
    }

    const tooltipDiv = d3
        .select('body')
        .append('div')
        .attr('id', 'htltooltip')
        .style('padding', '0.5em')
        .style('background', '#fff')
        .style('position', 'absolute')
        .style('border', '1px solid black')
        .style('border-radius', '5px')
        .style('opacity', 1);

    svg
        .append('g')
        .selectAll('.eventpointer')
        .data(graph.events)
        .enter()
        .append('path')
        .attr('class', 'eventpointer')
        .attr('id', d => 'path' + d.year)
        .attr('d', markerPath)
        .style('fill', 'red')
        .style('opacity', 0.5);

    // XXX: d3.on('mouseover') doesn't seem to work on this selection,
    // use plain DOM API instead
    for (const path of document.querySelectorAll('.eventpointer')) {
        path.addEventListener('mouseover', function (evt) {
            const d = d3.select(this).data()[0];
            d3.select(this).style('opacity', 1).style('cursor', 'pointer');
            tooltipDiv.transition()
                .duration(200)
                .style('opacity', .9);
            tooltipDiv.html(d.description)
                .style('left', `${evt.layerX}px`)
                .style('top', `${evt.layerY - 80}px`);
        });
        path.addEventListener('click', function () {
            const d = d3.select(this).data()[0];
            d3.select(this).style('opacity', 0.5).style('cursor', 'default');
            tooltipDiv.transition().style('opacity', '0')
            history.push(localResourceURI(d.event));
        });
        path.addEventListener('mouseleave', function () {
            d3.select(this).style('opacity', 0.5).style('cursor', 'default');
            tooltipDiv.transition().duration(500).style('opacity', '0')
        });
    }
    graph.height += 20;

    // Add link legend
    let legend = svg
        .append('g')
        .attr('class', 'legend')
        .style('font-size', '12px')
        .selectAll('path');
    legend = legend
        .data([
            { label: 'lien chronologique', color: '#22FF22' },
            { label: 'lien hierarchique', color: '#2222FF' },
            { label: 'lien partitif', color: '#FF2222' },
        ])
        .enter()
        .append('g');
    legend
        .append('rect')
        .attr('x', (d, i) => 10 + i * 200)
        .attr('y', graph.height + 10)
        .attr('height', 20)
        .attr('width', 20)
        .attr('fill', d => d.color)
        .attr('fill-opacity', 0.2);
    legend
        .append('text')
        .attr('x', (d, i) => 35 + i * 200)
        .attr('y', graph.height + 25)
        .attr('text-anchor', 'left')
        .text(d => d.label);

    // extend graph height to display legend
    graph.height += 35;
}

// * insert new lines betweens words greater than 3 characters
// * limit text to 4 lines
function d3EnhanceBoxLabels() {
    const el = d3.select(this),
        lines = [];
    let concat = false;
    for (const word of el.text().split(' ')) {
        if (concat) {
            lines[lines.length - 1] += ' ' + word;
        } else {
            if (lines.length === 4) {
                // reached maximum number of line
                lines[lines.length - 1] += '...';
                break;
            }
            lines.push(word);
        }
        concat = word.length <= 3;
    }
    el.text('');

    lines.forEach((line, i) => {
        el
            .append('tspan')
            .text(line)
            .attr('x', d => d.left + d.width / 2)
            .attr('y', d => d.top + 15 * i)
            .attr('text-anchor', 'middle')
            .attr('dy', '15');
    });
}

function formatDateRange(d) {
    return `${d.begin || '...'}-${d.end && d.end !== MAX_YEAR ? d.end : '...'}`;
}
