import React from 'react';
import { render } from 'react-dom';
import { BrowserRouter as Router, Route, Switch } from 'react-router-dom';

import { App, NotFound } from './components';
import { RDFResource } from './components/resource';
import { SearchResults, RICResources } from './components/home';
import { PageWithNavigation } from './components/editorial';
import { PortalStats } from './components/stats';
import { SparqlEditor } from './components/sparql';
import { Alignments } from './components/alignments';
import { localResourceURI } from './sparql';

const appElement = document.getElementById('app');

class redirectToExternal extends React.Component {
    componentWillMount() {
        this.props.history.push(
            localResourceURI('http://piaaf.demo.logilab.fr' + this.props.location.pathname),
        );
    }

    render() {
        return <RDFResource uri={'http://piaaf.demo.logilab.fr' + this.props.location.pathname} />;
    }
}

render(
    <Router>
        <App>
            <Switch>
                <Route exact path="/" render={() => <PageWithNavigation pageid="index" />} />
                <Route exact path="/editorial/:pageid" component={PageWithNavigation} />
                <Route exact path="/search" component={SearchResults} />
                <Route exact path="/sparql" component={SparqlEditor} />
                <Route exact path="/alignments" component={Alignments} />
                <Route exact path="/ric/:type" component={RICResources} />
                <Route exact path="/external/:uri" component={RDFResource} />
                <Route exact path="/stats" component={PortalStats} />
                <Route path="/resource/" component={redirectToExternal} />
                <Route path="*" component={NotFound} />
            </Switch>
        </App>
    </Router>,
    appElement,
);
