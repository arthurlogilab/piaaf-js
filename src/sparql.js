/* global SPARQL_ENDPOINT_URL, Headers, fetch */

import 'whatwg-fetch';

import standardDefs from './rqldef';

// static definition of node's color
const NODE_COLORS = {
    'ric:GroupType': '#AA0',
    'ric:CorporateBody': '#A0A',
    'ric:Person': '#0AA',
    'ric:Agent': '#AAA',
    'ric:Place': '#440',
    'ric:Position': '#404',
    'ric:RecordSet': '#044',
};

export const GRAPH_NAMES = {
    'urn:piaaf:data': 'Piaaf',
    'urn:piaaf:an': 'AN',
    'urn:piaaf:bnf': 'BnF',
    'urn:piaaf:siaf': 'SIAF',
    'dila': 'DILA',
};

// Return an URI to see the given resource in the application
export function localResourceURI(uri) {
    return `/external/${encodeURIComponent(uri)}`;
}

export function sparql(query) {
    //console.error('SPARQL', query);
    const url = `${SPARQL_ENDPOINT_URL}?query=${encodeURIComponent(query)}`;
    const headers = new Headers({ Accept: 'application/sparql-results+json' });
    return fetch(url, { headers: headers, credentials: 'include' })
        .then(response => {
            if (response.ok) {
                return response.json();
            }
            throw new Error(`unexpected response ${response}`);
        })
        .then(data => {
            //console.error('SPARQL results', data.results.bindings.length);
            return data.results.bindings;
        })
        .catch(error => {
            console.error(`SPARQL query ${query} failed with error:\n`, error);
            return {};
        });
}

export function fetchEntityReifiedRelations(uri) {
    return sparql(`
        PREFIX owl: <http://www.w3.org/2002/07/owl#>
        PREFIX ric: <http://www.ica.org/standards/RiC/ontology#>

        SELECT ?leftp ?r ?start ?stop ?description ?rightp ?y ?ylabel
        WHERE {
            VALUES ?rtypes {
                ric:AgentControlRelation
                ric:AgentLeadershipRelation
                ric:AgentHierarchicalRelation
                ric:AgentMembershipRelation
                ric:AgentTemporalRelation
                ric:AgentWholePartRelation
                ric:ArchivalProvenanceRelation
                ric:BusinessRelation
                ric:DenominationRelation
                ric:FindingAid
                ric:LocationRelation
                ric:SocialRelation
                ric:TypeRelation
            }
            BIND (<${uri}> AS ?x)
            ?x ?leftp ?r.
            ?r ?rightp ?y.
            ?y rdfs:label ?ylabel.
            ?r rdf:type ?rtypes.

            OPTIONAL {
                ?r ric:beginningDate ?start
            }
            OPTIONAL {
                ?r ric:endDate ?stop
            }
            OPTIONAL {
                ?r ric:description ?description
            }
            FILTER (?x != ?y)
            FILTER (?rightp != rdf:type)
        }
    `);
}

export function describe(uri) {
    // Given an entity's URI, return its attributes, subject and object relations as
    //
    //  {'attributes': {prop1: [{<value>, ...], prop2: ...},
    //   'subject-relations': {sprop1: [(<label>, <value>)]},
    //   'object-relations': {oprop2: [(<label>, <value>)]}
    //   }
    const describeP = sparql(`
SELECT ?s ?p ?o ?label WHERE {
    {
        BIND (<${uri}> as ?s)
        ?s ?p ?o
        OPTIONAL {?o rdfs:label ?RDFSLabel.}
        OPTIONAL {?o skos:prefLabel ?SKOSLabel.}
        BIND (COALESCE(?SKOSLabel, ?RDFSLabel) as ?label)
    } UNION {
        BIND (<${uri}> as ?o)
        ?s ?p ?o
        OPTIONAL {?s rdfs:label ?RDFSLabel.}
        OPTIONAL {?s skos:prefLabel ?SKOSLabel.}
        BIND (COALESCE(?SKOSLabel, ?RDFSLabel) as ?label)
        FILTER NOT EXISTS {
            ?o ?p2 ?s.
            ?p owl:inverseOf ?p2.
        }
        FILTER NOT EXISTS {
            ?o ?p3 ?s.
            ?p3 owl:inverseOf ?p.
        }
    }
}`);
    const reifiedRelationsP = fetchEntityReifiedRelations(uri);
    return Promise.all([describeP, reifiedRelationsP]).then(([bindings, reifiedBindings]) =>
        describeJsonGraph2relations(uri, bindings, reifiedBindings)
    );
}

export function fetchSameAsOtherGraphs(uri) {
    if (uri.startsWith('http://lannuaire')) {
        return _fetchSameAsOtherGraphsFromDila(uri);
    }
    return Promise.all([_fetchSameAsOtherGraphs(uri), _fetchSameAsDila(uri)]).then(
        ([altUris, dilaUri]) => Object.assign({}, altUris, dilaUri)
    );
}

function _buildGraphSameAsObject(bindings) {
    const altUris = {};
    for (const { graph, alt } of bindings) {
        if (GRAPH_NAMES[graph.value] !== undefined) {
            altUris[graph.value] = {
                uri: alt.value,
                localuri: localResourceURI(alt.value),
            };
        }
    }
    return altUris;
}

function _fetchSameAsOtherGraphsFromDila(uri) {
    return sparql(`
    SELECT ?graph ?alt WHERE {
        BIND (<${uri}> AS ?self)
        ?self owl:sameAs ?alt.
        GRAPH ?graph {
            ?alt a ?foo.
        }
    }
        `).then(bindings => _buildGraphSameAsObject(bindings));
}

function _fetchSameAsOtherGraphs(uri) {
    return sparql(`
    SELECT ?graph ?alt WHERE {
        BIND (<${uri}> AS ?self)
        ?self owl:sameAs ?x.
        GRAPH ?graph {
            ?alt owl:sameAs ?x.
        }
    }
    `).then(bindings => _buildGraphSameAsObject(bindings));
}

function _fetchSameAsDila(uri) {
    return sparql(`
SELECT ?alt WHERE {
    BIND (<${uri}> AS ?self)
    ?alt owl:sameAs ?self.
    FILTER REGEX(?alt, '^http://lannuaire.service-public.fr')
}
    `).then(bindings => {
        if (bindings.length) {
            const [{ alt }] = bindings;
            return {
                'dila': {
                    uri: alt.value,
                    localuri: localResourceURI(alt.value),
                },
            };
        }
        return null;
    });
}

export function fetchPrevNext(uri) {
    return sparql(`
PREFIX ric: <http://www.ica.org/standards/RiC/ontology#>

SELECT ?prev ?prevLabel ?next ?nextLabel
WHERE {
    ?selfProxy ric:proxyFor <${uri}>.
    OPTIONAL {
        ?selfProxy ric:hasNext ?nextProxy.
        ?nextProxy ric:proxyFor ?next.
        ?next rdfs:label ?nextLabel.
    }
    OPTIONAL {
        ?prevProxy ric:proxyFor ?prev.
        ?prevProxy ric:hasNext ?selfProxy.
        ?prev rdfs:label ?prevLabel.
    }
} LIMIT 1
    `).then(bindings => {
        if (bindings.length) {
            const binding = bindings[0];
            const prevnext = { prev: null, next: null };
            if (binding.prev) {
                prevnext.prev = {
                    uri: localResourceURI(binding.prev.value),
                    label: binding.prevLabel.value || binding.prev.value,
                };
            }
            if (binding.next) {
                prevnext.next = {
                    uri: localResourceURI(binding.next.value),
                    label: binding.nextLabel.value || binding.next.value,
                };
            }
            return prevnext;
        }
    });
}

function iterBindings(bindings) {
    const allUnpacked = [];
    for (const row of bindings) {
        const unpacked = {};
        for (const [propname, valuedescr] of Object.entries(row)) {
            let value = valuedescr ? valuedescr.value : null;
            if (value && valuedescr.type === 'uri') {
                value = reprefix(value);
            }
            unpacked[propname] = value;
        }
        // yield unpacked;
        allUnpacked.push(unpacked);
    }
    return allUnpacked;
}

function packReifiedBindings(bindings) {
    const byProps = {};
    const packedRelation = {};
    for (const { leftp, r, rightp, start, stop, description, y, ylabel }
        of iterBindings(bindings)) {
        if (byProps[leftp] === undefined) {
            byProps[leftp] = [];
        }
        if (packedRelation[r] === undefined) {
            const reldef = {
                start,
                stop,
                description,
                relationuri: r,
                startyear: parseYearString(start),
                relations: [],
            }
            byProps[leftp].push(reldef);
            packedRelation[r] = reldef;
        }
        packedRelation[r].relations.push({
            rightp,
            desturi: y,
            destlabel: ylabel,
        })
    }
    for (const key of Object.keys(byProps)) {
        byProps[key].sort((a, b) => a.startyear - b.startyear);
    }
    return byProps;
}

// XXX export for testing purpose only
export function describeJsonGraph2relations(uri, bindings, reifiedBindings) {
    // DESCRIBE returns s / p / o bindings, each being object with type/value
    // properties.
    //
    // Given the described uri and associated bindings, this function return the
    // higher-level structure below:
    //
    //  {'attributes': {prop1: [{<value>, ...], prop2: ...},
    //   'subject-relations': {sprop1: [(<label>, <value>)]},
    //   'object-relations': {oprop2: [(<label>, <value>)]}
    //   }
    const attributes = {},
        subject_relations = {},
        object_relations = {};

    function push(container, prop, value_binding, label_binding) {
        if (container[prop] === undefined) {
            container[prop] = [];
        }
        container[prop].push([
            label_binding ? label_binding.value : value_binding.value,
            value_binding.value,
        ]);
    }

    function pushAttr(container, prop, value_binding) {
        if (container[prop] === undefined) {
            container[prop] = [];
        }
        container[prop].push(value_binding.value);
    }

    for (const binding of bindings) {
        const prop = reprefix(binding.p.value);
        if (binding.o.type === 'literal' || binding.o.type === 'typed-literal') {
            pushAttr(attributes, prop, binding.o);
        } else if (binding.s.value === uri) {
            push(subject_relations, prop, binding.o, binding.label);
        } else if (binding.o.value === uri) {
            push(object_relations, prop, binding.s, binding.label);
        } else {
            console.error('unexpected triple', binding, 'not matching', uri);
        }
    }
    const packedReified = packReifiedBindings(reifiedBindings);
    for (const relname of Object.keys(subject_relations)) {
        if (packedReified[relname] !== undefined) {
            delete subject_relations[relname];
        }
    }
    return {
        attributes: attributes,
        'subject-relations': subject_relations,
        'object-relations': object_relations,
        'packed-relations': packedReified,
    };
}

// Return data from the given query with a structure suitable to sigmajs:
// {nodes: [n1...], edges: [e1...]}
//
// Given SPARQL query is expected to define `entity`, `target` and `rel1` variables.
export function graphData(query, fromBaseSet) {
    return sparql(query).then(bindings => sparqlGraph2SigmaGraphData(bindings, fromBaseSet));
}

// Given bindings where we expect to find `entity`, `entityLabel`, `target`,
// `targetLabel`, `rel1` and ?graph variables, return a structure suitable to
// sigmajs: {nodes: [n1...], edges: [e1...]} where each node and edge contains
// information as required/supported by sigmajs as well as additional
// information we need to provide features such as graph filtering..

export function sparqlGraph2SigmaGraphData(bindings, fromBaseSet) {
    const graph = { nodes: [], edges: [] },
        nodeIndex = {};

    function pushNode(resource, label, resourceType, fromDate, toDate) {
        let rdfType;
        if (resourceType !== undefined) {
            rdfType = reprefix(resourceType.value);
        } else {
            rdfType = '<inconnu>';
        }
        if (nodeIndex[resource.value] === undefined) {
            let color = NODE_COLORS[rdfType];
            if (color === undefined) {
                color = '#444';
            }
            const node = {
                id: resource.value,
                // use _label instead of label so it's not considered by sigmajs
                _label: label ? label.value : resource.value,
                rdfType: rdfType,
                fromDate: parseYear(fromDate),
                toDate: parseYear(toDate),
                // putting nodes on a several lines initially seems to
                // give the best results with the layout algorithm
                x: graph.nodes.length,
                y: graph.nodes.length % 2,
                color: color,
                // set of graphs from which relations from/to this node belong
                rdfGraphs: new Set(),
            };
            nodeIndex[resource.value] = node;
            node.fromBaseSet = fromBaseSet(node);
            node.size = node.fromBaseSet ? 2 : 1;
            node.type = node.fromBaseSet ? 'star' : 'def';
            graph.nodes.push(node);
        }
    }

    let count = 0;
    for (const binding of bindings) {
        pushNode(
            binding.entity,
            binding.entityLabel,
            binding.entityType,
            binding.entityFromDate,
            binding.entityToDate,
        );
        pushNode(
            binding.target,
            binding.targetLabel,
            binding.targetType,
            binding.targetFromDate,
            binding.targetToDate,
        );
        const relationType = reprefix(binding.rel1.value),
            rdfGraph = binding.graph ? binding.graph.value : '<graphe inconnu>';

        graph.edges.push({
            id: count,
            source: binding.entity.value,
            target: binding.target.value,
            rdfGraph: rdfGraph,
            rdfType: relationType,
            beginningDate: parseYear(binding.relBeginningDate),
            endDate: parseYear(binding.relEndDate),
            color: _getColor(relationType),
            type: 'arrow',
            size: 1, // this is necessary of edge hovering to work properly
        });
        count += 1;
        for (const uri of [binding.entity.value, binding.target.value]) {
            const node = nodeIndex[uri];
            node.rdfGraphs.add(rdfGraph);
        }
    }
    return graph;
}

// build the query to get results expected by the above function. It's expected
// to be given a `binding` stream to be inserted into SPARQL 'BIND' clause,
// usually to bind either ?entity (resource view context) or ?entityType (type
// view context).
export function sparqlGraphQuery(binding) {
    return `
PREFIX piaaf: <http://piaaf.demo.logilab.fr/ontology#>
PREFIX ric: <http://www.ica.org/standards/RiC/ontology#>
PREFIX owl: <http://www.w3.org/2002/07/owl#>
PREFIX skos: <http://www.w3.org/2004/02/skos/core#>

SELECT ?entity ?entityType ?entityLabel
       ?target ?targetType ?targetLabel
       ?rel1 ?relBeginningDate ?relEndDate ?graph
       ?entityFromDate ?entityToDate
       ?targetFromDate ?targetToDate
WHERE {
    BIND (${binding})
    {

      GRAPH ?graph {?entity ?rel1 ?relEntity.}
      ?relEntity ?rel2 ?target;
                 a ?relType.
      ?relType rdfs:ancestorClass ric:Relation.
      ?target a ?targetType.
      FILTER(
        ?targetType NOT IN (owl:Class, ric:AgentName)
        AND ?entity != ?target
        AND NOT EXISTS{?rel1 owl:inverseOf ?rel2}
        AND NOT EXISTS{?rel2 owl:inverseOf ?rel1}
      )
      OPTIONAL {?relEntity ric:beginningDate ?relBeginningDate.}
      OPTIONAL {?relEntity ric:endDate ?relEndDate.}

    } UNION {

      GRAPH ?graph {?entity ?rel1 ?target.}
      FILTER(
        ?rel1 IN (owl:sameAs,
                  ric:groupType, ric:occupiedBy,
                  ric:member, ric:memberOf,
                  piaaf:hasFunctionOfType, piaaf:isFunctionTypeOf)
      )
      OPTIONAL {?target a ?targetType.}

    }

    ?entity a ?entityType.

    OPTIONAL {?entity rdfs:label ?entityRDFSLabel.}
    OPTIONAL {?entity skos:prefLabel ?entitySKOSLabel.}
    BIND (COALESCE(?entitySKOSLabel, ?entityRDFSLabel) as ?entityLabel)

    OPTIONAL {?target rdfs:label ?targetLabel.}

    OPTIONAL {?entity ric:beginningDate ?entityBeginningDate.}
    OPTIONAL {?entity ric:birthDate ?entityBirthDate.}
    BIND (COALESCE(?entityBirthDate, ?entityBeginningDate) as ?entityFromDate)
    OPTIONAL {?entity ric:endDate ?entityEndDate.}
    OPTIONAL {?entity ric:deathDate ?entityDeathDate.}
    BIND (COALESCE(?entityDeathDate, ?entityEndDate) as ?entityToDate)

    OPTIONAL {?target ric:beginningDate ?targetBeginningDate.}
    OPTIONAL {?target ric:birthDate ?targetBirthDate.}
    BIND (COALESCE(?targetBirthDate, ?targetBeginningDate) as ?targetFromDate)
    OPTIONAL {?target ric:endDate ?targetEndDate.}
    OPTIONAL {?target ric:deathDate ?targetDeathDate.}
    BIND (COALESCE(?targetDeathDate, ?targetEndDate) as ?targetToDate)
}`;
}

// similar as the above but starting from concepts in a given scheme.
export function sparqlSKOSGraphQuery(scheme) {
    return `
PREFIX ric: <http://www.ica.org/standards/RiC/ontology#>
PREFIX owl: <http://www.w3.org/2002/07/owl#>
PREFIX skos: <http://www.w3.org/2004/02/skos/core#>

SELECT ?entity ?entityType ?entityLabel
       ?target ?targetType ?targetLabel
       ?rel1 ?relBeginningDate ?relEndDate ?graph
       ?entityFromDate ?entityToDate
       ?targetFromDate ?targetToDate
WHERE {
    BIND (skos:Concept as ?entityType)
    GRAPH ?graph {?entity ?rel1 ?relEntity.}
    ?entity skos:inScheme <${scheme}>.
    ?relEntity ?rel2 ?target;
               a ?relType.
    ?relType rdfs:ancestorClass ric:Relation.
    ?target a ?targetType.
    FILTER(
      ?targetType NOT IN (owl:Class, ric:AgentName)
      AND ?entity != ?target
      AND NOT EXISTS{?rel1 owl:inverseOf ?rel2}
      AND NOT EXISTS{?rel2 owl:inverseOf ?rel1}
    )
    OPTIONAL {?relEntity ric:beginningDate ?relBeginningDate.}
    OPTIONAL {?relEntity ric:endDate ?relEndDate.}

    OPTIONAL {?entity skos:prefLabel ?entityLabel.}

    OPTIONAL {?target rdfs:label ?targetLabel.}

    OPTIONAL {?entity ric:beginningDate ?entityBeginningDate.}
    OPTIONAL {?entity ric:birthDate ?entityBirthDate.}
    BIND (COALESCE(?entityBirthDate, ?entityBeginningDate) as ?entityFromDate)
    OPTIONAL {?entity ric:endDate ?entityEndDate.}
    OPTIONAL {?entity ric:deathDate ?entityDeathDate.}
    BIND (COALESCE(?entityDeathDate, ?entityEndDate) as ?entityToDate)

    OPTIONAL {?target ric:beginningDate ?targetBeginningDate.}
    OPTIONAL {?target ric:birthDate ?targetBirthDate.}
    BIND (COALESCE(?targetBirthDate, ?targetBeginningDate) as ?targetFromDate)
    OPTIONAL {?target ric:endDate ?targetEndDate.}
    OPTIONAL {?target ric:deathDate ?targetDeathDate.}
    BIND (COALESCE(?targetDeathDate, ?targetEndDate) as ?targetToDate)
}`;
}

// cache of attributed colors {key: color}
const COLOR_CACHE = {
    agentControlledBy: '#D6EB76',
    agentFollows: '#4A0751',
    agentHasLeadershipRelation: '#83DF9F',
    agentHasMembershipRelation: '#BD5D12',
    agentHasWholePartRelation: '#125A18',
    agentIsInferiorInHierarchicalRelation: '#34E443',
    agentIsSuperiorInHierarchicalRelation: '#865FBC',
    agentPrecedes: '#73A15F',
    groupHasMember: '#38C6BE',
    groupHasPart: '#9ED88D',
    groupType: '#C0250F',
    hasAgentControlRelation: '#85D307',
    hasAuthorityRelation: '#7117DB',
    hasFunctionOfType: '#F13EA5',
    hasLocationRelation: '#4C3235',
    hasPart: '#7CD2D5',
    hasProvenanceRelation: '#A21B6B',
    hasSocialRelation: '#43A193',
    hasTypeRelation: '#B89D74',
    hasWholePartRelation: '#371625',
    isFunctionTypeOf: '#52502D',
    isTypeOf: '#233760',
    leadBy: '#18121D',
    member: '#D09921',
    occupiedBy: '#2AC08A',
    originatedBy: '#8ACF72',
    sameAs: '#929A22',
    thingFollows: '#560AA2',
    thingPrecedes: '#763D29',
    underAuthorityRelation: '#3D0CF5',
};

function _getColor(key) {
    let value = COLOR_CACHE[key];
    if (value === undefined) {
        value = _getRandomColor();
        COLOR_CACHE[key] = value;
    }
    return value;
}

function _getRandomColor() {
    const letters = '0123456789ABCDEF';
    let color = '#';
    for (let i = 0; i < 6; i++) {
        color += letters[Math.floor(Math.random() * 16)];
    }
    return color;
}

// known prefixes, without '#' which is expected to be found as separator
// between the prefix and the local identifier
const _PREFIXES = {
    'http://piaaf.demo.logilab.fr/ontology': 'piaaf',
    'http://www.ica.org/standards/RiC/ontology': 'ric',
    'http://isni.org/ontology': 'isni',
    'http://www.w3.org/2004/02/skos/core': 'skos',
    'http://www.w3.org/2002/07/owl': 'owl',
    'http://www.w3.org/2000/01/rdf-schema': 'rdfs',
    'http://www.w3.org/1999/02/22-rdf-syntax-ns': 'rdf',

    'http://www.df.gouv.fr/administration_francaise/annuaire': 'annuaire',
    'http://www.mondeca.com/system/basicontology': 'mondeca-basicontology',
    'http://www.mondeca.com/system/t3': 'mondeca-t3',
};

// search for known prefix in the given qualified name and return it using a
// prefix if found
export function reprefix(qname) {
    const nameParts = qname.split('#');
    const prefix = _PREFIXES[nameParts[0]];
    if (prefix !== undefined) {
        qname = prefix + ':' + nameParts[1];
    }
    return qname;
}

function parseYearString(yearStr) {
    if (yearStr) {
        const year = yearStr.split('-')[0];
        return parseInt(year); // use parseInt to accept non-digit trailing chars;
    }
    return yearStr
}

export function parseYear(binding, defaultValue) {
    if (defaultValue === undefined) {
        defaultValue = null;
    }
    if (!binding) {
        return defaultValue;
    }
    let year;
    if (binding.datatype === 'http://www.w3.org/2001/XMLSchema#date') {
        year = binding.value.split('-')[0];
    } else if (binding.datatype === 'http://www.w3.org/2001/XMLSchema#gYear') {
        year = binding.value.split('-')[0];
    } else if (typeof binding.value === 'string' && /^\d+$/.test(binding.value)) {
        year = Number(binding.value);
    } else {
        console.error('unhandled datatype for year', binding.datatype);
        return defaultValue;
    }
    return parseInt(year);
}

// Object containing documentation for properties defined in the ontology:
//
//   {reprefix(property): rdfs-comment's value}
//
// to be used to display help in the UI
const RELATIONS_HELP = Object.assign({}, standardDefs);

// feed RELATIONS_HELP from the data-store
sparql(`
PREFIX owl: <http://www.w3.org/2002/07/owl#>

SELECT ?prop ?labelFr ?commentFr ?labelEn ?commentEn WHERE {
    {?prop a owl:Class} UNION {?prop a owl:DatatypeProperty} UNION {?prop a owl:ObjectProperty}
    OPTIONAL {
        ?prop rdfs:label ?labelFr.
        FILTER(LANG(?labelFr) = 'fr')
     }
     OPTIONAL {
        ?prop rdfs:comment ?commentFr
        FILTER(LANG(?commentFr) = 'fr')
    }
    OPTIONAL {
        ?prop rdfs:label ?labelEn.
        FILTER(LANG(?labelEn) = 'en')
    }
    OPTIONAL {
        ?prop rdfs:comment ?commentEn
        FILTER(LANG(?commentEn) = 'en')
   }
}

`).then(bindings => {
    for (const { prop, labelFr, commentFr, labelEn, commentEn } of bindings) {
        const quri = reprefix(prop.value);
        if (RELATIONS_HELP[quri] !== undefined) {
            // Give precende to local, hand-written label translations over
            // ontology ones but keep comment from ontology.
            RELATIONS_HELP[quri].comment = {
                fr: commentFr ? commentFr.value : null,
                en: commentEn ? commentEn.value : null,
            };
        } else {
            RELATIONS_HELP[quri] = {
                label: {
                    fr: labelFr ? labelFr.value : null,
                    en: labelEn ? labelEn.value : null,
                },
                comment: {
                    fr: commentFr ? commentFr.value : null,
                    en: commentEn ? commentEn.value : null,
                },
            };
        }
    }
});

export function propLabel(propname, fallback = propname) {
    if (RELATIONS_HELP[propname] !== undefined) {
        return RELATIONS_HELP[propname].label.fr || RELATIONS_HELP[propname].label.en || fallback;
    }
    return fallback;
}

export function propComment(propname, fallback = '') {
    if (RELATIONS_HELP[propname] !== undefined && RELATIONS_HELP[propname].comment !== undefined) {
        return (
            RELATIONS_HELP[propname].comment.fr || RELATIONS_HELP[propname].comment.en || fallback
        );
    }
    return fallback;
}

// Return a promise on list of queries (`{title, sparql}`) to be displayed in
// the interface
export function listQueries(kind) {
    return sparql(
        `
PREFIX piaaf: <http://piaaf.demo.logilab.fr/ontology#>

SELECT ?label ?sparql
WHERE {
  ?q a piaaf:${kind};
     piaaf:SPARQL ?sparql;
     rdfs:label ?label.
}
ORDER BY ?label
`,
        { credentials: 'include' },
    ).then(bindings => {
        return bindings.map(binding => {
            return {
                title: binding.label.value,
                sparql: binding.sparql.value,
            };
        });
    });
}
