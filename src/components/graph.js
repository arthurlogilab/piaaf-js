import React from 'react';

import { sigma as Sigma } from 'sigma';
import 'sigmaForceLayoutWorker';
import 'sigmaForceLayoutSupervisor';
import 'sigmaShapeLibrary';
import 'sigmaCustomShapes';

import { ReactBootstrapSlider } from 'react-bootstrap-slider';

import { graphData, localResourceURI, propLabel, propComment } from '../sparql';

const PRECHECKED_ETYPES = {
    'ric:TopRecordSet': new Set(['ric:RecordSet']),
    'ric:RecordSet': new Set(['ric:TopRecordSet']),
    'ric:FunctionAbstract': new Set(['ric:CorporateBody']),
    'ric:GroupType': new Set(['ric:CorporateBody', 'ric:FunctionAbstract']),
    'ric:Position': new Set(['ric:Person']),
    'ric:Person': new Set(['ric:CorporateBody']),
};

function precheckedFactory(rdfType) {
    const precheckedTypes = PRECHECKED_ETYPES[rdfType] || new Set();
    return nodeType => precheckedTypes.has(nodeType);
}

export class SigmaRDFGraph extends React.Component {
    constructor(props) {
        super(props);
        this.state = this.initialState(props.query, props.fromBaseSet);
        this.sigmaGraph = null;
        this.filterGraph = this.filterGraph.bind(this);
        this.dateRangeFacetChanged = this.dateRangeFacetChanged.bind(this);
        this.switchFullscreen = this.switchFullscreen.bind(this);
        this.prechecked = this.props.inputType ? precheckedFactory(this.props.inputType) : null;
    }

    initialState(query, fromBaseSet) {
        return {
            query: query,
            fromBaseSet: fromBaseSet,
            graph: null,
            displayedGraphs: null,
            displayedResourceTypes: null,
            displayedRelationTypes: null,
            dateRange: null,
            displayedDateRange: null,
            fullscreen: false,
        };
    }

    componentDidMount() {
        this.fetchData(this.state.query, this.state.fromBaseSet);
    }

    componentWillReceiveProps(nextProps) {
        if (nextProps.query !== this.props.query) {
            this.setState(this.initialState(null, null));
            this.fetchData(nextProps.query, nextProps.fromBaseSet);
        }
    }

    componentWillUnmount() {
        if (this.sigmaGraph) {
            this.sigmaGraph.killForceAtlas2();
        }
    }

    render() {
        return (
            <div id="graphAndControls">
                <div id="graphControls">
                    <div>
                        <ColorizedSelectorFacet
                            title="Graphes"
                            className="blackFont"
                            displayed={this.state.displayedGraphs}
                            facetUpdated={this.filterGraph} />
                        <ColorizedSelectorFacet
                            title="Ressources"
                            displayed={this.state.displayedResourceTypes}
                            facetUpdated={this.filterGraph} />
                        <ColorizedSelectorFacet
                            title="Relations"
                            displayed={this.state.displayedRelationTypes}
                            facetUpdated={this.filterGraph} />
                    </div>
                </div>
                <div id="graphContainer">
                    <span
                        onClick={this.switchFullscreen}
                        id="fullscreenSwitch"
                        className={`glyphicon glyphicon-${
                            this.state.fullscreen ? 'resize-small' : 'fullscreen'
                        }`}
                        title={
                            this.state.fullscreen
                                ? 'Quitter le plein écran'
                                : 'Passer en plein écran'
                        } />
                    {this.state.dateRange && (
                        <div id="dateSlider">
                            <span className="graphControlTitle">Dates</span>
                            <ReactBootstrapSlider
                                min={this.state.dateRange[0]}
                                max={this.state.dateRange[1]}
                                value={this.state.displayedDateRange}
                                slideStop={this.dateRangeFacetChanged}
                                tooltip="always" />
                        </div>
                    )}
                </div>
                <div className="popover" id="popover" />
            </div>
        );
    }

    switchFullscreen() {
        const div = document.querySelector('#graphAndControls');
        let fullscreen;
        if (
            document.fullscreenElement ||
            document.webkitFullscreenElement ||
            document.mozFullScreenElement ||
            document.msFullscreenElement
        ) {
            if (document.exitFullscreen) {
                document.exitFullscreen();
            } else if (document.webkitExitFullscreen) {
                document.webkitExitFullscreen();
            } else if (document.mozCancelFullScreen) {
                document.mozCancelFullScreen();
            } else if (document.msExitFullscreen) {
                document.msExitFullscreen();
            }
            fullscreen = false;
        } else {
            if (div.requestFullscreen) {
                div.requestFullscreen();
            } else if (div.webkitRequestFullscreen) {
                div.webkitRequestFullscreen();
            } else if (div.mozRequestFullScreen) {
                div.mozRequestFullScreen();
            } else if (div.msRequestFullscreen) {
                div.msRequestFullscreen();
            }
            fullscreen = true;
        }
        this.setState({ fullscreen });
        this.sigmaGraph.killForceAtlas2();
        this.startLayout();
    }

    fetchData(query, fromBaseSet) {
        graphData(query, fromBaseSet).then(graph => {
            const container = document.getElementById('graphAndControls');
            if (graph.nodes.length > 0) {
                this.sigmaGraph = sigmaInit(graph, this.sigmaGraph);
                // ensure the graph's div container is displayed
                container.style.display = 'block';
            } else {
                // hide the graph's div container
                container.style.display = 'none';
            }
            const displayedGraphs = initSelectorFacetData(graph.edges, 'rdfGraph', _ => ({
                    color: '#FFF',
                })),
                displayedResourceTypes = initSelectorFacetData(
                    graph.nodes,
                    'rdfType',
                    item => ({ color: item.color }),
                    this.prechecked,
                ),
                displayedRelationTypes = initSelectorFacetData(graph.edges, 'rdfType', item => ({
                    color: item.color,
                }));
            // compute min/max begin/end date in selection's edges
            let beginningDateRange = [null, null],
                endDateRange = [null, null];
            for (const node of graph.nodes) {
                beginningDateRange = updateRange(beginningDateRange, node.fromDate);
                endDateRange = updateRange(endDateRange, node.toDate);
            }
            for (const edge of graph.edges) {
                beginningDateRange = updateRange(beginningDateRange, edge.beginningDate);
                endDateRange = updateRange(endDateRange, edge.endDate);
            }
            const dateRange = outerRange(beginningDateRange, endDateRange),
                displayedDateRange = dateRange;
            this.setState({
                query,
                fromBaseSet,
                graph,
                displayedGraphs,
                displayedResourceTypes,
                displayedRelationTypes,
                dateRange,
                displayedDateRange,
            });
            this.filterGraph();
        });
    }

    // filter graph nodes and edges according to user inputs
    filterGraph() {
        const displayedGraphs = this.state.displayedGraphs,
            displayedResourceTypes = this.state.displayedResourceTypes,
            displayedRelationTypes = this.state.displayedRelationTypes,
            displayedDateRange = this.state.displayedDateRange;
        const sigmaGraphGraph = this.sigmaGraph.graph,
            renderer = this.sigmaGraph.renderers[0];
        // first kill the layout and renderer, they will have to be refreshed to
        // properly consider changes to the graph
        this.sigmaGraph.killForceAtlas2();
        this.sigmaGraph.killRenderer(renderer);
        // reset active count of displayed* data structures
        resetFacetDataActiveCount(displayedResourceTypes);
        resetFacetDataActiveCount(displayedGraphs);
        resetFacetDataActiveCount(displayedRelationTypes);
        // compute list of displayed graphs
        const displayedGraphsSet = [];
        for (const [graphUri, values] of Object.entries(this.state.displayedGraphs)) {
            if (values.displayed) {
                displayedGraphsSet.push(graphUri);
            }
        }
        // then process nodes
        for (const node of this.state.graph.nodes) {
            if (
                (!displayedResourceTypes[node.rdfType].displayed && !node.fromBaseSet) ||
                !displayedGraphsSet.some(graph => node.rdfGraphs.has(graph)) ||
                !rangeOverlap([node.fromDate, node.toDate], displayedDateRange)
            ) {
                const sigmaNode = sigmaGraphGraph.nodes(node.id);
                if (sigmaNode !== undefined) {
                    // update coordinates for use on later reinsertion
                    node.x = sigmaNode.x;
                    node.y = sigmaNode.y;
                    sigmaGraphGraph.dropNode(node.id);
                }
            } else {
                // make the node appear again if it was hidden
                if (sigmaGraphGraph.nodes(node.id) === undefined) {
                    sigmaGraphGraph.addNode(node);
                }
                // update facet data's active count
                displayedResourceTypes[node.rdfType].activeCount += 1;
            }
        }
        // then drop edges hidden by the user or because source or target node is
        // not in the displayed graph
        for (const edge of this.state.graph.edges) {
            if (
                !displayedRelationTypes[edge.rdfType].displayed ||
                !displayedGraphs[edge.rdfGraph].displayed ||
                !rangeOverlap([edge.beginningDate, edge.endDate], displayedDateRange)
            ) {
                // edge hidden by the user
                if (sigmaGraphGraph.edges(edge.id) !== undefined) {
                    sigmaGraphGraph.dropEdge(edge.id);
                }
            } else {
                if (
                    sigmaGraphGraph.nodes(edge.source) === undefined ||
                    sigmaGraphGraph.nodes(edge.target) === undefined
                ) {
                    // edge hidden because source or target isn't displayed
                    if (sigmaGraphGraph.edges(edge.id) !== undefined) {
                        sigmaGraphGraph.dropEdge(edge.id);
                    }
                } else {
                    // make the edge appear again if it was hidden
                    if (sigmaGraphGraph.edges(edge.id) === undefined) {
                        sigmaGraphGraph.addEdge(edge);
                    }
                    // update facet data's active count
                    displayedGraphs[edge.rdfGraph].activeCount += 1;
                    displayedRelationTypes[edge.rdfType].activeCount += 1;
                }
            }
        }
        // now refresh the graph and update state
        renderer.options.camera = this.sigmaGraph.camera;
        this.sigmaGraph.addRenderer(renderer.options);
        this.sigmaGraph.refresh();
        this.setState({
            displayedGraphs: displayedGraphs,
            displayedResourceTypes: displayedResourceTypes,
            displayedRelationTypes: displayedRelationTypes,
            displayedDateRange: displayedDateRange,
        });
        this.startLayout();
    }

    dateRangeFacetChanged(event) {
        // modify state without setState intentionally, state update will be
        // triggered by filterGraph
        //
        // eslint-disable-next-line react/no-direct-mutation-state
        this.state.displayedDateRange = event.target.value;
        this.filterGraph();
    }

    startLayout() {
        this.sigmaGraph.startForceAtlas2({
            scalingRatio: 10,
            outboundAttractionDistribution: true,
            gravity: 10,
            //strongGravityMode: true,
            //edgeWeightInfluence: 0,
            //barnesHutTheta: 0.5,
            //slowdown: 1,
            //linLogMode: false,
        });
        const nbNodes = this.sigmaGraph.graph.nodes().length;
        let timeout;
        if (nbNodes > 1000) {
            timeout = 10;
        } else if (nbNodes > 100) {
            timeout = 5;
        } else {
            timeout = 2;
        }
        window.setTimeout(_ => this.sigmaGraph.stopForceAtlas2(), timeout * 1000);
    }
}

function sigmaInit(graph, previousSigmaGraph) {
    const rendererSettings = { container: 'graphContainer' };
    // conditionally enable edge hovering: it's only supported by the canvas
    // renderer which is not usable when we have a too big graph to display
    let enableEdgeHovering;
    if (graph['edges'].length < 200) {
        rendererSettings.type = 'canvas';
        enableEdgeHovering = true;
    } else if (!webgl_support()) {
        rendererSettings.type = 'canvas';
        enableEdgeHovering = true;
        console.warn('webgl not supported');
    } else {
        rendererSettings.type = 'webgl';
        enableEdgeHovering = false;
    }
    if (previousSigmaGraph === null) {
        const sigmaGraph = new Sigma({
            graph,
            settings: { enableEdgeHovering, clone: false },
            renderer: rendererSettings,
        });
        // display a pop-over on node containing a link to the entity
        sigmaGraph.bind('overNode', function (e) {
            const node = e.data.node,
                html = `<a href="${localResourceURI(node.id)}">${node._label}</a>`,
                prefix = sigmaXYPrefix(sigmaGraph);
            showPopOver(html, node[prefix + 'x'], node[prefix + 'y']);
        });
        sigmaGraph.bind('overEdge', function (e) {
            const edge = e.data.edge,
                n1 = sigmaGraph.graph.nodes(edge.source),
                n2 = sigmaGraph.graph.nodes(edge.target),
                prefix = sigmaXYPrefix(sigmaGraph),
                relativeX = Math.abs(n1[prefix + 'x'] + n2[prefix + 'x']) / 2,
                relativeY = Math.abs(n1[prefix + 'y'] + n2[prefix + 'y']) / 2;
            let html = edge.rdfType;
            if (edge.beginningDate || edge.endDate) {
                html += `<br/>${edge.beginningDate || ''} - ${edge.endDate || ''}`;
            }
            showPopOver(html, relativeX, relativeY);
        });
        sigmaGraph.bind(
            'clickNode',
            e => (document.location.href = localResourceURI(e.data.node.id)),
        );
        sigmaGraph.bind('clickStage', function () {
            const popover = document.getElementById('popover');
            popover.style.display = 'none';
        });
        return sigmaGraph;
    } else {
        // XXX only kill renderer when necessary?
        previousSigmaGraph.settings('enableEdgeHovering', enableEdgeHovering);
        previousSigmaGraph.killForceAtlas2();
        previousSigmaGraph.killRenderer(previousSigmaGraph.renderers[0]);
        previousSigmaGraph.graph.clear();
        previousSigmaGraph.graph.read(graph);
        // avoid creation of a new camera with the new renderer
        rendererSettings.camera = previousSigmaGraph.camera;
        previousSigmaGraph.addRenderer(rendererSettings);
        previousSigmaGraph.refresh();
        return previousSigmaGraph;
    }
}

// Return the prefix that sigmajs use to store x/y coordinates in the node object
function sigmaXYPrefix(sigmaGraph) {
    return sigmaGraph.renderers[0].options.type === 'canvas'
        ? sigmaGraph.renderers[0].options.prefix
        : sigmaGraph.camera.prefix;
}

// Display a popover containing given `html` at position `relativeX`/`relativeY`
// in the graph canvas
function showPopOver(html, relativeX, relativeY) {
    const popover = document.getElementById('popover'),
        container = document.getElementById('graphContainer'),
        containerBoundaries = container.getBoundingClientRect();
    popover.innerHTML = html;
    // relativeX and relativeY are the coordinate in the canvas independantly of
    // scrolling, so we have to consider this for proper location of the popover
    // (getBoundingClientRect is scroll aware though)
    popover.style.left = window.scrollX + relativeX + containerBoundaries.left + 'px';
    popover.style.top = window.scrollY + relativeY + containerBoundaries.top + 'px';
    popover.style.display = 'inline';
}

// Display a list of colorized items that may be clicked to filter the items
// displayed in the graph
function ColorizedSelectorFacet(props) {
    let entries = [],
        nbDisplayed = 0,
        nbHidden = 0;

    if (props.displayed) {
        entries = Object.entries(props.displayed);
        entries.sort();
    }

    for (const [_, attrs] of entries) {
        if (!attrs.fromBaseSet) {
            if (attrs.displayed) {
                nbDisplayed += 1;
            } else {
                nbHidden += 1;
            }
        }
    }
    const nextToggleDisplayStatus = nbHidden > nbDisplayed,
        toggleHelp = nextToggleDisplayStatus
            ? 'cliquez pour tout sélectionner'
            : 'cliquez pour tout désélectionner';

    function toggleClicked() {
        for (const [_, attrs] of entries) {
            attrs.displayed = nextToggleDisplayStatus;
        }
        props.facetUpdated();
    }

    function facetClicked(event) {
        const target =
                event.target.localName === 'span' ? event.target.parentElement : event.target,
            value = target.dataset.graphvalue,
            displayData = props.displayed[value];
        displayData.displayed = !displayData.displayed;
        props.facetUpdated();
    }

    return (
        <div>
            <div className="graphControlTitle" onClick={toggleClicked} title={toggleHelp}>
                <span
                    className={`glyphicon glyphicon-${
                        nextToggleDisplayStatus ? 'unchecked' : 'check'
                    }`} />
                {props.title}
            </div>
            <div className={props.className}>
                {Array.prototype.map.call(entries, ([key, attrs]) => (
                    <div
                        className={attrs.fromBaseSet ? 'graphSelectorFromBaseSet' : 'graphSelector'}
                        onClick={!attrs.fromBaseSet && facetClicked}
                        data-graphvalue={key}
                        title={propComment(key)}
                        key={key}
                        style={{ backgroundColor: attrs.color }} >
                        <span
                            className={`glyphicon glyphicon-${
                                attrs.fromBaseSet ? 'star' : attrs.displayed ? 'check' : 'unchecked'
                            }`} />
                        {propLabel(key)} ({attrs.activeCount}/{attrs.totalCount})
                    </div>
                ))}
            </div>
        </div>
    );
}

// Return {key: {displayed: bool, totalCount: int, fromBaseSet: bool, ...}} from a
// list of graph items (node or edge), where value used as key is specified
// using the `key` argument.
//
// The `buildCallback` function must be given to initialize the object that will be
// associated to key, usually with at least a 'color' property set.
function initSelectorFacetData(items, key, buildCallback, prechecked) {
    const displayed = {};
    for (const item of items) {
        if (displayed[item[key]] === undefined) {
            displayed[item[key]] = buildCallback(item);
            displayed[item[key]].totalCount = 1;
        } else {
            displayed[item[key]].totalCount += 1;
        }
        displayed[item[key]].activeCount = 0;
        // take care there may be inconsistant fromBaseSet result accross
        // items aggregated under a single key > priority to false
        if (!item.fromBaseSet) {
            displayed[item[key]].fromBaseSet = false;
            displayed[item[key]].displayed = prechecked ? prechecked(item.rdfType) : true;
        } else if (displayed[item[key]].fromBaseSet === undefined) {
            displayed[item[key]].fromBaseSet = true;
            displayed[item[key]].displayed = true;
        }
    }
    return displayed;
}

function resetFacetDataActiveCount(displayed) {
    for (const key of Object.keys(displayed)) {
        displayed[key].activeCount = 0;
    }
}

function updateRange(range, value) {
    if (value !== null && !isNaN(value)) {
        range[0] = safeMin(range[0], value);
        range[1] = Math.max(range[1], value);
    }
    return range;
}

// Return true if range given as first argument overlap with the range given as
// second argument
export function rangeOverlap(range, displayedRange) {
    // return true if the displayed range is unspecified or if the considered
    // range start/end are both null
    if (displayedRange === null || (range[0] === null && range[1] === null)) {
        return true;
    }
    // if range start is null, return true if end is greater than displayed range start
    if (range[0] === null) {
        return range[1] >= displayedRange[0];
    }
    // if range end is null, return true if start is lower than displayed range end
    if (range[1] === null) {
        return range[0] <= displayedRange[1];
    }
    // no null, return true if both conditions are met
    return range[0] <= displayedRange[1] && range[1] >= displayedRange[0];
}

// Given two ranges [min, max] or null, return a new date range with the outer
// boundaries or null if both arguments are null)
export function outerRange(range1, range2) {
    let min = null,
        max = null;
    if (range1) {
        min = range1[0];
        max = range1[1];
    }
    if (range2) {
        min = safeMin(min, range2[0]);
        max = Math.max(max, range2[1]);
    }
    if (min === null) {
        return null;
    }
    return [min, max];
}

// Same as Math.min but properly handling null values
export function safeMin(val1, val2) {
    if (val1 === null) {
        return val2;
    }
    if (val2 === null) {
        return val1;
    }
    return Math.min(val1, val2);
}

// https://stackoverflow.com/questions/11871077/proper-way-to-detect-webgl-support#22953053
function webgl_support() {
    try {
        const canvas = document.createElement('canvas');
        return (
            !!window.WebGLRenderingContext &&
            (canvas.getContext('webgl') || canvas.getContext('experimental-webgl'))
        );
    } catch (e) {
        return false;
    }
}
