import React from 'react';
import * as d3 from 'd3';

import { last, partialRight } from 'lodash';
import { sparql, propLabel } from '../sparql';
import { sparqlUrl } from './sparql';
import { Loading } from './helpers';

const GRAPH_NAMES = {
    'urn:piaaf:data': 'Piaaf',
    'urn:piaaf:an': 'AN',
    'urn:piaaf:bnf': 'BnF',
    'urn:piaaf:siaf': 'SIAF',
};

const barCharHeight = 200;

const GRAPH_COLOURS = {
    // cf. http://tools.medialab.sciences-po.fr/iwanthue/
    an: '#b6546f',
    data: '#997a34',
    siaf: '#796db0',
    piaaf: '#964ecd',
};

const TYPES_OF_INTEREST = [
    'Agent',
    'CorporateBody',
    'Event',
    'FindingAid',
    'FunctionAbstract',
    'GroupType',
    'Mandate',
    'Person',
    'Place',
    'Position',
    'Record',
    'RecordSet',
];

const etypeColour = d3.scaleOrdinal(d3.schemeCategory20c);

function graphBasename(graphname) {
    return last(graphname.split(':'));
}

function graphColour(graphname) {
    const basename = graphBasename(graphname);
    return GRAPH_COLOURS[basename] || GRAPH_COLOURS['piaaf'];
}

function graphstatDict(initvalue = 0) {
    const stats = {};
    for (const graph of Object.keys(GRAPH_NAMES)) {
        stats[graph] = initvalue;
    }
    return stats;
}

function bindingValues(bindings) {
    const allValues = []
    for (const row of bindings) {
        const values = {};
        for (const key of Object.keys(row)) {
            values[key] = row[key].value;
        }
        allValues.push(values);
    }
    return allValues;
}

function parseYear(year, defaultValue = null) {
    if (year === null) {
        return defaultValue;
    }
    try {
        return Number(year.split('-')[0]);
    } catch (e) {
        return defaultValue;
    }
}

function statsByYear(recordsetStats) {
    const nullStats = graphstatDict();
    let minyear = 3000,
        maxyear = 0;

    for (const graph in recordsetStats) {
        for (const { firstyear, lastyear } of recordsetStats[graph]) {
            if (firstyear !== null && lastyear !== null) {
                minyear = Math.min(firstyear, minyear);
                maxyear = Math.max(lastyear, maxyear);
            }
        }
    }

    const yearStats = [];
    for (let year = minyear; year <= maxyear; year++) {
        yearStats.push(graphstatDict());
    }

    for (const graph in recordsetStats) {
        for (const { firstyear, lastyear, count } of recordsetStats[graph]) {
            if (firstyear === null && lastyear === null) {
                nullStats[graph] += Number(count);
            } else {
                for (let year = firstyear; year <= lastyear; year++) {
                    yearStats[year - minyear][graph] += Number(count);
                }
            }
        }
    }

    return {
        null: nullStats,
        yearstats: yearStats,
        minyear,
        maxyear,
    };
}

async function fetchRecordSetStats() {
    const bindings = await sparql(`
        PREFIX ric: <http://www.ica.org/standards/RiC/ontology#>

        SELECT ?graph ?begdate ?enddate (count(?x) as ?count) WHERE {
            GRAPH ?graph {
                ?x a ric:RecordSet.
                OPTIONAL {
                    ?x ric:beginningDate ?begdate.
                }
                OPTIONAL {
                    ?x ric:endDate ?enddate.
                }
                # FILTER (NOT EXISTS {?x ric:memberOf ?y})
            }
        } GROUP BY ?graph ?begdate ?enddate
    `);
    const stats = {};
    for (const { graph, begdate, enddate, count } of bindingValues(bindings)) {
        if (stats[graph] === undefined) {
            stats[graph] = [];
        }
        stats[graph].push({
            firstyear: parseYear(begdate),
            lastyear: parseYear(enddate),
            count,
        });
    }
    return stats;
}

async function fetchEtypeStats() {
    const etypes = TYPES_OF_INTEREST.map(etype => `ric:${etype}`);
    const bindings = await sparql(`
        PREFIX ric: <http://www.ica.org/standards/RiC/ontology#>

        SELECT ?graph ?type (count(?x) as ?count) WHERE {
        VALUES ?etypes {${etypes.join(' ')}}
        GRAPH ?graph {
            ?x a ?type ;
            rdf:type ?etypes.
        }
        } GROUP BY ?graph ?type
    `);
    const stats = {};
    for (const { graph, type, count } of bindingValues(bindings)) {
        if (stats[graph] === undefined) {
            stats[graph] = {};
        }
        stats[graph][type.split('#')[1]] = count;
    }
    return stats;
}

function recordsetBarchart(g, stats, { width, height }) {
    let maxrecords = 0;
    for (const ystats of stats.yearstats) {
        // NOTE: Object.values() is not supported on IE
        const yrecords = Object.keys(ystats)
            .map(k => ystats[k])
            .reduce((x, y) => x + y);
        if (maxrecords < yrecords) {
            maxrecords = yrecords;
        }
    }

    const x = d3
        .scaleLinear()
        .range([0, width])
        .domain([stats.minyear, stats.maxyear + 1]);

    const bandwidth = x(stats.minyear + 1) - x(stats.minyear);
    const y = d3
        .scaleLinear()
        .rangeRound([height, 0])
        .domain([0, maxrecords]);

    const stackKeys = Object.keys(GRAPH_NAMES);

    g
        .append('g')
        .selectAll('g')
        .data(d3.stack().keys(stackKeys)(stats.yearstats))
        .enter()
        .append('g')
        .attr('class', d => `bar bar${graphBasename(d.key)}`)
        .attr('fill', function (d) {
            return graphColour(d.key);
        })
        .selectAll('rect')
        .data(function (d) {
            return d;
        })
        .enter()
        .append('rect')
        .attr('x', function (d, i) {
            return x(stats.minyear + i);
        })
        .attr('y', function (d) {
            return y(d[1]);
        })
        .attr('height', function (d) {
            return y(d[0]) - y(d[1]);
        })
        .attr('width', bandwidth);

    g
        .append('g')
        .attr('class', 'axis')
        .attr('transform', 'translate(0,' + height + ')')
        .call(d3.axisBottom(x).tickFormat(d3.format('4')));

    g
        .append('g')
        .attr('class', 'axis')
        .call(d3.axisLeft(y).ticks(null, 's'))
        .append('text')
        .attr('x', 2)
        .attr('y', y(y.ticks().pop()) + 0.5)
        .attr('dy', '0.32em')
        .attr('fill', '#000')
        .attr('font-weight', 'bold')
        .attr('text-anchor', 'start')
        .text("Nombre de composants d'archives");

    const legend = g
        .append('g')
        .attr('font-family', 'sans-serif')
        .attr('font-size', 10)
        .attr('text-anchor', 'end')
        .selectAll('g')
        .data(stackKeys.slice().reverse())
        .enter()
        .append('g')
        .attr('transform', function (d, i) {
            return 'translate(0,' + i * 20 + ')';
        });

    legend
        .append('rect')
        .attr('x', width - 19)
        .attr('width', 19)
        .attr('height', 19)
        .attr('fill', graphColour);

    legend
        .append('text')
        .attr('x', width - 24)
        .attr('y', 9.5)
        .attr('dy', '0.32em')
        .text(function (d) {
            return GRAPH_NAMES[d];
        });
}

function etypeStatsDonut(selection, history) {
    selection.each(function (data) {
        const total = data.map(item => item.count).reduce((a, b) => Number(a) + Number(b));
        const pxRatio = 0.5; // number of pixel for each counted element
        const radius = total / (2 * Math.PI) * pxRatio,
            width = radius * 2,
            height = width,
            margin = { top: 10, right: 10, bottom: 10, left: 10 },
            countFormat = d3.format('4');
        const donutid = `donut${graphBasename(data[0].graphname)}`;

        const g = selection
            .append('g')
            .attr('id', donutid)
            .attr('width', width + margin.left + margin.right)
            .attr('height', height + margin.top + margin.bottom);
        // .attr('transform', 'translate(' + width / 2 + ',' + height / 2 + ')');
        const pie = d3
            .pie()
            .value(function (d) {
                return countFormat(d.count);
            })
            .sort(null);

        const arc = d3
            .arc()
            .outerRadius(radius * 0.8)
            .innerRadius(radius * 0.7)
            .cornerRadius(5)
            .padAngle(0.03);

        // this arc is used for aligning the text labels
        const outerArc = d3
            .arc()
            .outerRadius(radius * 0.9)
            .innerRadius(radius * 0.9);

        g.append('g').attr('class', 'slices');
        g.append('g').attr('class', 'labelName');
        g.append('g').attr('class', 'lines');

        g
            .select('.slices')
            .datum(data)
            .selectAll('path')
            .data(pie)
            .enter()
            .append('path')
            .attr('class', d => `arc arc${graphBasename(d.data.graphname)} arc${d.data.label}`)
            .attr('fill', function (d) {
                return etypeColour(d.data.label);
            })
            .attr('d', arc);
        // add text labels
        g
            .select('.labelName')
            .selectAll('text')
            .data(pie)
            .enter()
            .append('text')
            .attr('class', d => `countlabel countlabel${graphBasename(d.data.graphname)} countlabel${d.data.label}`)
            .attr('dy', '.35em')
            .style('visibility', 'hidden')
            .html(function (d) {
                const etype = `ric:${d.data.label}`;
                // add "key: value" for given category. Number inside tspan is bolded in stylesheet.
                return `${propLabel(etype)} : <tspan> ${countFormat(d.data.count)}</tspan>`;
            })
            .attr('transform', function (d) {
                // effectively computes the centre of the slice.
                // see https://github.com/d3/d3-shape/blob/master/README.md#arc_centroid
                const pos = outerArc.centroid(d);
                // changes the point to be on left or right depending on where label is.
                pos[0] = radius * 0.95 * (midAngle(d) < Math.PI ? 1 : -1);
                return 'translate(' + pos + ')';
            })
            .style('text-anchor', function (d) {
                // if slice centre is on the left, anchor text to start, otherwise anchor to end
                return midAngle(d) < Math.PI ? 'start' : 'end';
            });

        // add lines connecting labels to slice. A polyline creates straight lines
        // connecting several points
        g
            .select('.lines')
            .selectAll('polyline')
            .data(pie)
            .enter()
            .append('polyline')
            .attr('class', d => `labelguide labelguide${graphBasename(d.data.graphname)} labelguide${d.data.label}`)
            .style('visibility', 'hidden')
            .style('fill', 'transparent')
            .style('stroke', '#000')
            .attr('points', function (d) {
                // see label transform function for explanations of these three lines.
                const pos = outerArc.centroid(d);
                pos[0] = radius * 0.95 * (midAngle(d) < Math.PI ? 1 : -1);
                return [arc.centroid(d), outerArc.centroid(d), pos];
            });

        d3.selectAll(`#${donutid} .slices path`).call(toolTip);
        // calculates the angle for the middle of a slice
        function midAngle(d) {
            return d.startAngle + (d.endAngle - d.startAngle) / 2;
        }

        function onPathEnter(data) {
            d3.select(this).style('cursor', 'pointer');
            g
                .append('text')
                .attr('class', 'toolCircle')
                .attr('dy', -15) // hard-coded. can adjust this to adjust text vertical alignment in tooltip
                .html(toolTipHTML(data)) // add text to the circle.
                .style('font-size', '.9em')
                .style('text-anchor', 'middle'); // centres text in tooltip

            g
                .append('circle')
                .attr('class', 'toolCircle')
                .attr('r', radius * 0.55) // radius of tooltip circle
                .style('fill', etypeColour(data.data.label)) // colour based on category mouse is over
                .style('fill-opacity', 0.35);
            d3.selectAll(`#${donutid} .labelguide`).style('visibility', 'visible');
            d3.selectAll(`#${donutid} .countlabel`).style('visibility', 'visible');
            highlightRecordSets(data.data.graphname);
        }

        // function that creates and adds the tool tip to a selected element
        function toolTip(selection) {
            // add tooltip (svg circle element) when mouse enters label or slice
            selection.on('click', d => history.push(sparqlUrl(d.data.label, d.data.graphname)));
            selection.on('mouseenter', onPathEnter);
            // remove the tooltip when mouse leaves the slice/label
            selection.on('mouseout', function () {
                d3.select(this).style('cursor', 'default');
                d3.selectAll('.toolCircle').remove();
                d3.selectAll(`#${donutid} .labelguide`).style('visibility', 'hidden');
                d3.selectAll(`#${donutid} .countlabel`).style('visibility', 'hidden');
                unhighlightRecordSets();
            });
        }

        // function to create the HTML string for the tool tip. Loops through each
        // key in data object and returns the html string key: value
        function toolTipHTML(data) {
            return `
                <tspan x="0">${data.data.count} ${data.data.label}</tspan>
            `;
        }

        g
            .append('text')
            .html(GRAPH_NAMES[data[0].graphname]) // add text to the circle.
            .attr('dy', 5)
            .attr('fill', graphColour(data[0].graphname))
            .on('mouseenter', () => { highlightStats(data[0].graphname); highlightRecordSets(data[0].graphname); })
            .on('mouseout', () => { unHighlightStats(); unhighlightRecordSets(); })
            .style('text-decoration', 'underline')
            .style('font-weight', 'bold')
            .style('font-size', '.9em')
            .style('text-anchor', 'middle'); // centres text in tooltip
    });
}

function etypeLegends(g) {
    const legend = g
        .append('g')
        .attr('id', 'etypelegend')
        .attr('font-family', 'sans-serif')
        .attr('font-size', 10)
        .attr('text-anchor', 'end')
        .selectAll('g')
        .data(TYPES_OF_INTEREST)
        .enter()
        .append('g')
        .attr('transform', function (d, i) {
            return 'translate(0,' + (50 + (i * 20)) + ')';
        });

    const width = 900,
        rectWidth = 19;

    legend
        .append('rect')
        .attr('x', width - rectWidth)
        .attr('width', rectWidth)
        .attr('height', rectWidth)
        .attr('fill', etypeColour)
        .on('mouseenter', d => highlightEtype(d))
        .on('mouseout', d => unHighlightEtype(d));

    legend
        .append('text')
        .attr('x', width - rectWidth - 24)
        .attr('y', 9.5)
        .attr('dy', '0.32em')
        .text(function (d) {
            return propLabel(`ric:${d}`);
        });
    return legend;
}

function highlightEtype(etype) {
    d3.selectAll('.arc').style('opacity', '0.3');
    d3.selectAll(`.arc.arc${etype}`).style('opacity', '1')
    d3.selectAll('.countlabel').style('visibility', 'hidden');
    d3.selectAll(`.countlabel${etype}`).style('visibility', 'visible');
    d3.selectAll('.labelguide').style('visibility', 'hidden');
    d3.selectAll(`.labelguide${etype}`).style('visibility', 'visible');
}

function unHighlightEtype() {
    d3.selectAll('.arc').style('opacity', '1');
    d3.selectAll('.countlabel').style('visibility', 'hidden');
    d3.selectAll('.labelguide').style('visibility', 'hidden');
}

function highlightRecordSets(graphname) {
    d3.selectAll('.bar').style('opacity', '0.3');
    d3.selectAll(`.bar.bar${graphBasename(graphname)}`).style('opacity', '1')
}

function unhighlightRecordSets() {
    d3.selectAll('.bar').style('opacity', '1');
}

function highlightStats(graphname) {
    d3.selectAll('.labelguide').style('visibility', 'hidden');
    d3.selectAll(`.labelguide${graphBasename(graphname)}`).style('visibility', 'visible');
    d3.selectAll('.countlabel').style('visibility', 'hidden');
    d3.selectAll(`.countlabel${graphBasename(graphname)}`).style('visibility', 'visible');
}

function unHighlightStats() {
    d3.selectAll('.labelguide').style('visibility', 'hidden');
    d3.selectAll('.countlabel').style('visibility', 'hidden');
}

function drawBarChart(container, recordsetStats) {
    if (container === null) {
        // safety belt, might happen when page changes with react router
        return;
    }

    const svg = d3.select(container),
        margin = { top: 20, right: 20, bottom: 30, left: 40 },
        width = +svg.attr('width') - margin.left - margin.right,
        // height = +svg.attr('width') - margin.top - margin.bottom,
        g = svg.append('g').attr('transform', 'translate(' + margin.left + ',' + margin.top + ')');

    recordsetBarchart(g.append('g').attr('class', 'barchart'), statsByYear(recordsetStats), {
        width,
        height: barCharHeight,
    });
}

function drawPieCharts(container, etypeStats, history) {
    if (container === null) {
        // safety belt, might happen when page changes with react router
        return;
    }

    const svg = d3.select(container),
        margin = { top: 20, right: 20, bottom: 30, left: 40 },
        g = svg.append('g').attr('transform', 'translate(' + margin.left + ',' + margin.top + ')');

    const centers = {
        'urn:piaaf:bnf': [450, 400],
        'urn:piaaf:siaf': [600, 150],
        'urn:piaaf:an': [200, 150],
    };
    for (const [graphname, [centerx, centery]] of Object.entries(centers)) {
        const data = [];
        for (const [label, count] of Object.entries(etypeStats[graphname])) {
            data.push({ graphname, label, count });
        }
        g
            .append('g')
            .attr('class', 'donuts')
            .attr('transform', `translate(${centerx},${centery})`)
            .datum(data)
            .call(partialRight(etypeStatsDonut, history));
    }
    etypeLegends(g);
}

export class PortalStats extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            recordsetStats: null,
            etypeStats: null,
        }
    }

    componentDidMount() {
        this.fetchData();
    }

    fetchData() {
        Promise.all([
            fetchRecordSetStats(),
            fetchEtypeStats(),
        ]).then(([recordsetStats, etypeStats]) => {
            this.setState({ recordsetStats, etypeStats });
        });
    }

    render() {
        if (this.state.recordsetStats === null) {
            return <Loading />;
        }
        return (
            <div id="stats">
                <h2>Répartition dans le temps des groupes de documents</h2>
                <div id="barchart">
                    <svg
                        ref={svg => drawBarChart(svg, this.state.recordsetStats,
                                                 this.props.history)}
                        width="960" height="300">
                    </svg>
                </div>
                <h2>Répartition des ressources par catégorie</h2>
                <div id="piecharts">
                    <svg
                        ref={svg =>  drawPieCharts(svg, this.state.etypeStats, this.props.history)}
                        width="960" height="600">
                    </svg>
                </div>
            </div>
        );
    }
}